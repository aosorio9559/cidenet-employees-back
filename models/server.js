require("dotenv").config();
const express = require("express");
const cors = require("cors");
const connectDatabase = require("../databases/config.db");

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 3000;
    this.employeesPath = "/api/employees";
    this.areasPath = "/api/areas";
    this.countriesPath = "/api/countries";
    this.idTypesPath = "/api/idTypes";
    this.statusPath = "/api/status";
    this.connectDb();
    this.middlewares();
    this.routes();
  }

  async connectDb() {
    await connectDatabase();
  }

  routes() {
    this.app.use(this.employeesPath, require("../routes/employee.route"));
    this.app.use(this.areasPath, require("../routes/area.route"));
    this.app.use(this.countriesPath, require("../routes/country.route"));
    this.app.use(this.idTypesPath, require("../routes/idType.route"));
    this.app.use(this.statusPath, require("../routes/status.route"));
  }

  middlewares() {
    /* Enable CORS */
    this.app.use(cors());
    /* Read and parse body */
    this.app.use(express.json());
    /* Public directory */
    this.app.use(express.static("public"));
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`Listening on port: ${this.port}`);
    });
  }
}

module.exports = Server;
