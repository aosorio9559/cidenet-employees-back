const { Schema, model } = require("mongoose");

const IdTypeSchema = new Schema({
  name: {
    type: String,
  },
});

module.exports = model("idType", IdTypeSchema, "idTypes");
