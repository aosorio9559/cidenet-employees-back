const { Schema, model } = require("mongoose");

const AreaSchema = new Schema({
  name: {
    type: String,
  },
});

module.exports = model("area", AreaSchema);
