const { Schema, model } = require("mongoose");
// const uniqueValidator = require("mongoose-unique-validator");

const EmployeeSchema = new Schema({
  firstName: {
    type: String,
    required: [true, "El primer nombre es requerido"],
  },
  secondName: {
    type: String,
  },
  lastName: {
    type: String,
    required: [true, "El primer apellido es requerido"],
  },
  secondLastName: {
    type: String,
    required: [true, "El segundo apellido es requerido"],
  },
  country: {
    type: String,
    required: [true, "El país de empleo  es requerido"],
  },
  idType: {
    type: String,
    required: [true, "El tipo de identificación es requerido"],
  },
  idNumber: {
    type: String,
    required: [true, "El número de identificación es requerido"],
  },
  email: {
    type: String,
    required: [true, "El correo electrónico  es requerido"],
    unique: true,
  },
  entranceDate: {
    type: Date,
    required: [true, "La fecha de ingreso requerida"],
  },
  area: {
    type: String,
    required: [true, "El área es requerida"],
  },
  status: {
    type: String,
    required: [true, "El estado es requerido"],
  },
  registerDate: {
    type: String,
    required: [true, "La fecha de registro es requerida"],
  },
  deleted: {
    type: Boolean,
    default: false,
  },
});

// EmployeeSchema.plugin(uniqueValidator, {
//   message: "{PATH} debe ser único",
// });

module.exports = model("employee", EmployeeSchema);
