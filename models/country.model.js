const { Schema, model } = require("mongoose");

const CountrySchema = new Schema({
  name: {
    type: String,
  },
  code: {
    type: String,
  },
});

module.exports = model("countrie", CountrySchema);
