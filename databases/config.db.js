const mongoose = require("mongoose");

async function connectDatabase() {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,
    });

    console.log("Database online!");
  } catch (error) {
    console.error(error);
    throw new Error("An error ocurred while connecting to the database");
  }
}

module.exports = connectDatabase;
