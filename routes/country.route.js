const { Router } = require("express");
const { getAllCountries } = require("../controllers/country.controller");

const router = Router();

router.get("/", getAllCountries);

module.exports = router;
