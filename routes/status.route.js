const { Router } = require("express");
const { getAllStatus } = require("../controllers/status.controller");

const router = Router();

router.get("/", getAllStatus);

module.exports = router;
