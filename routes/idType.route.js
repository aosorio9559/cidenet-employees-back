const { Router } = require("express");
const { getAllIdTypes } = require("../controllers/idType.controller");

const router = Router();

router.get("/", getAllIdTypes);

module.exports = router;
