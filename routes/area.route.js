const { Router } = require("express");
const { getAllAreas } = require("../controllers/area.controller");

const router = Router();

router.get("/", getAllAreas);

module.exports = router;
