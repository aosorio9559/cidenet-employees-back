const { response } = require("express");
const IdType = require("../models/idType.model");

async function getAllIdTypes(req, res = response) {
  const allIdTypes = await IdType.find();

  return res.status(200).json(allIdTypes);
}

module.exports = {
  getAllIdTypes,
};
