const { response } = require("express");
const Country = require("../models/country.model");

async function getAllCountries(req, res = response) {
  const allCountries = await Country.find();

  return res.status(200).json(allCountries);
}

module.exports = {
  getAllCountries,
};
