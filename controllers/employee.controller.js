const { response, request } = require("express");
const Employee = require("../models/employee.model");

async function getAllEmployees(req, res = response) {
  const allEmployees = await Employee.find({ deleted: false });
  return res.status(200).json(allEmployees);
}

async function getEmployee(req = request, res = response) {
  const { id } = req.params;
  const employee = await Employee.findById(id);
  return res.status(200).json(employee);
}

async function createEmployee(req = request, res = response) {
  const body = req.body;
  const { email } = body;
  const emailExists = await Employee.findOne({ email });
  if (emailExists) {
    return res.status(400).json({
      ok: false,
      msg: "El correo ya existe",
    });
  }

  const employee = new Employee(body);
  await employee.save();

  return res.status(201).json({ ok: true });
}

async function updateEmployee(req = request, res = response) {
  const { body } = req;
  const { id } = req.params;

  await Employee.findByIdAndUpdate(id, body);

  return res.json({ ok: true });
}

async function deleteEmployee(req, res = response) {
  const { id } = req.params;
  await Employee.findByIdAndUpdate(id, { deleted: true });

  return res.json({ ok: true });
}

module.exports = {
  getAllEmployees,
  getEmployee,
  createEmployee,
  updateEmployee,
  deleteEmployee,
};
