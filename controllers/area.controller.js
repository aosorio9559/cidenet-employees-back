const { response } = require("express");
const Area = require("../models/area.model");

async function getAllAreas(req, res = response) {
  const allAreas = await Area.find();

  return res.status(200).json(allAreas);
}

module.exports = {
  getAllAreas,
};
