const { response } = require("express");
const Status = require("../models/status.model");

async function getAllStatus(req, res = response) {
  const allAStatus = await Status.find();

  return res.status(200).json(allAStatus);
}

module.exports = {
  getAllStatus,
};
